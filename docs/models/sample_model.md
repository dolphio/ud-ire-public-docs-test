---
layout: default
title: Sample Model
parent: Models
nav_order: 2
---

# Sample Model 
This model can be used to do some things.

<dl>
    <dt>Context</dt><dd>Sample Reporting</dd>
    <dt>Permissions/Roles</dt><dd>sample_role</dd>
    <dt>Row Level Permissions</dt><dd>Yes</dd>
    <dt>Refresh Schedule</dt><dd>Nightly</dd>
</dl>

## Tables

* [Sample Table]({{ "/docs/tables/sample_table.html" | relative_url }} )
