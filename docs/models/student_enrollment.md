---
layout: default
title: Student Enrollment
parent: Models
nav_order: 2
---

# Student Enrolment (Sample)
This model can be used to investigate current and historic student enrollment.

<dl>
    <dt>Context</dt><dd>Student Reporting</dd>
    <dt>Permissions/Roles</dt><dd>stu_enrollment</dd>
    <dt>Row Level Permissions</dt><dd>Yes</dd>
    <dt>Refresh Schedule</dt><dd>Nightly</dd>
</dl>

## Tables

* [Enrollment Fact](/docs/tables/enrollment_fact.md)
* [Student Data Dimension](/docs/tables/student_data.md)
* [Term Dimension](/docs/tables/term.md)