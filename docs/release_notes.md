---
layout: page
title: Introduction
permalink: /release_notes/
---

Here are the release notes

<ul>
  {% for post in site.posts %}
    <li>
      <a href="{{ post.url }}">{{ post.title }}</a>
    </li>
  {% endfor %}
</ul>