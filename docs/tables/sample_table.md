---
layout: default
title: Sample Table
parent: Tables
---

# Sample Table
this is a sample table


## Column 1
<dl>
    <dt>Usage</dt><dd>Dimension</dd>
    <dt>Type</dt><dd>Character(30)</dd>
    <dt>Source</dt><dd><code>ps_table.column</code></dd>
</dl>
This is a column.  I can be used to group things.


## Column 2
<dl>
    <dt>Usage</dt><dd>Fact</dd>
    <dt>Type</dt><dd>Number</dd>
    <dt>Source</dt><dd><code>ps_table.column</code></dd>
</dl>
This is a second column.  It can be summed.


## Column 3
<dl>
    <dt>Usage</dt><dd>Dimension</dd>
    <dt>Type</dt><dd>Character(30)</dd>
    <dt>Source</dt><dd><code>ps_table.column</code></dd>
</dl>
This is a 3rd column