---
layout: default
title: Student Enrollment Dashboard
parent: Dashboards
---

# Student Enrollment dashboard
[Open Dashboard](http://example.com/){: .btn .btn-blue }

This dashboard can be used to view Current and historical Student Enrollment

<dl>
    <dt>Model</dt><dd>Student Enrollment</dd>
    <dt>Context</dt><dd>Student Reporting</dd>
    <dt>Permissions/Roles</dt><dd>stu_enrollment</dd>
    <dt>Row Level Permissions</dt><dd>Yes</dd>
    <dt>Refresh Schedule</dt><dd>Nightly</dd>
</dl>

_See [Student Enrollment Model]( {{ "/docs/assets/style.css" | relative_url }} ) for full column definitions._


## Tab1
Basic info on tab1


## Tab2
Basic Info on tab2

## Tab3
Basic info on Tab 3